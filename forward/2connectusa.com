
$ORIGIN 2connectusa.com.

$TTL 7200
@	IN SOA	ns1.2connectbahrain.com. hostmaster.2connectbahrain.com. (
		2013092201 7200 1200 3600000 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
	172800	IN NS		ns3.2connectbahrain.com.
		IN MX	10	sm

; Sender policy framework
@		IN TXT  "v=spf1 mx ip4:80.88.242.27 a:sm.2connectusa.com ~all"

;
; Nameservers DNS
ns1             IN A    46.29.56.196
ns		IN A	80.88.242.3
ns		IN A	46.29.56.203
ns2		IN A	80.88.242.4
; End of Nameservers
;


smtp		IN CNAME	mail
imap		IN CNAME	mail
pop3		IN CNAME	mail
sm		IN A	80.88.242.27
mail		IN CNAME	mail.2connectbahrain.com

www		IN A	80.88.242.44
