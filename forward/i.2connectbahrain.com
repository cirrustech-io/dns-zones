
$TTL 7200
@	IN SOA	ns1 hostmaster (
		2014080701 7200 1200 1209600 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
		;IN MX	10	mail1.2connectbahrain.com.
		;IN MX	20	mail1.2connectbahrain.com.

		IN A	192.168.2.28
;wiki		IN A	192.168.2.191
;apps		IN A	192.168.2.92

;
; Intranet Servers

apps		IN A	192.168.2.92
backup		IN A 	192.168.2.29
opmanager	IN A	192.168.2.100
crm		IN A	192.168.2.18
office		IN A	192.168.2.33
vision		IN A	80.88.248.123
vision		IN A	192.168.16.2
smokeping	IN A	192.168.2.3
dynamips	IN A	192.168.2.21
inventory       IN A    192.168.2.42
netflow		IN A	192.168.2.35
me-netflow	IN A	192.168.2.36
kvm		IN A	192.168.2.22
pacer		IN A	192.168.2.34
maker		IN A	192.168.50.3
ubuntu-repo	IN A	192.168.2.61
eset		IN A	192.168.50.56
dns-master	IN A	192.168.2.56
observium	IN A	192.168.2.13
ids		IN A	192.168.2.38
netdot		IN A	192.168.2.53
intranet	IN A	192.168.2.28
gitlab		IN A	192.168.2.11
; End of intranet servers

