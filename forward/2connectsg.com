
$ORIGIN 2connectsg.com.

$TTL 7200
@	IN SOA	ns1.2connectbahrain.com. hostmaster.2connectbahrain.com. (
		2015180804 7200 1200 3600000 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
	172800	IN NS		ns3.2connectbahrain.com.
;		IN MX	10	hoster.2connectbahrain.com.
;		IN MX	10	sm-1

; Sender policy framework
;IN TXT "v=spf1 mx ip4:80.88.242.38 ip4:80.88.242.3 ip4:80.88.242.4 -all"
;@		IN TXT "v=spf1 mx ip4:80.88.242.36 a:rabbit.2connectsg.com -all"
;@		IN TXT  "v=spf1 mx ip4:80.88.242.27 a:sm.2connectsg.com ~all"

;
; Nameservers DNS
ns1             IN A    46.29.56.196
ns		IN A	80.88.242.3
ns		IN A	46.29.56.203
ns2		IN A	80.88.242.4
; End of Nameservers
;

;
smtp		IN CNAME	mail
imap		IN CNAME	mail
pop3		IN CNAME	mail
sm		IN A	80.88.242.27
sm-1		IN A	80.88.242.32
mail		IN CNAME	mail.2connectbahrain.com
; End of Email Related
;
;
; Testing Office 365 alias domain
;
;@	3600	IN TXT  "MS=ms32767452"
2connectsg.com.                           3600    IN MX           0 2connectsg-com.mail.protection.outlook.com.
autodiscover.2connectsg.com.              3600    IN CNAME        autodiscover.outlook.com.
2connectsg.com.                           3600    IN TXT          "v=spf1 include:spf.protection.outlook.com -all"
_sip._tls.2connectsg.com.                 3600    IN SRV          100 1 443 sipdir.online.lync.com.
_sipfederationtls._tcp.2connectsg.com.    3600    IN SRV          100 1 5061 sipfed.online.lync.com.
sip.2connectsg.com.                       3600    IN CNAME        sipdir.online.lync.com.
lyncdiscover.2connectsg.com.              3600    IN CNAME        webdir.online.lync.com.
msoid.2connectsg.com.                     3600    IN CNAME        clientconfig.microsoftonline-p.net.
enterpriseenrollment.2connectsg.com.      3600    IN CNAME        enterpriseenrollment.manage.microsoft.com.
enterpriseregistration.2connectsg.com.    3600    IN CNAME        enterpriseregistration.windows.net.
;
; End of Testing Office 365 alias domain
