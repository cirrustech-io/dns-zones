$TTL 300
@       IN SOA  ns1 hostmaster (
                2016080302 7200 1200 1209600 7200 )
        172800  IN NS           ns1.infonas.com.
        172800  IN NS           ns2.infonas.com.
                IN MX  10       mail.i.infonas.com.

mail		IN A		80.88.242.37
@		IN TXT 		"v=spf1 mx -all"

;
;
; Infrastructure Servers
;
vpn             IN A    80.88.241.126
ftp             IN A    80.88.242.5
ubuntu-repo     IN A    80.88.242.40
crm		IN A	80.88.242.29
dns-ptr-test	IN A	80.88.242.39
np		IN A	80.88.242.41
speedtest	IN A	80.88.242.23
speedtest-01	CNAME	speedtest
speedtest-02	CNAME	speedtest
;
; End of Infrastructure Servers
;
