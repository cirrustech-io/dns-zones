
$ORIGIN 2connectbahrain.com.

$TTL 300
@	IN SOA	ns1 hostmaster (
		2016080401 7200 1200 1209600 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
	172800	IN NS		ns3.2connectbahrain.com.

; Sender policy framework
;IN TXT "v=spf1 mx ip4:80.88.242.38 ip4:80.88.242.3 ip4:80.88.242.4 -all"
;@		IN TXT "v=spf1 mx ip4:80.88.242.36 a:rabbit.2connectbahrain.com -all"
;@		IN TXT  "v=spf1 mx ip4:80.88.242.27 a:sm.2connectbahrain.com ~all"

@		IN A	80.88.242.5

;
; Nameservers DNS
ns1             IN A    46.29.56.196
ns1             IN AAAA 2a01:87c0:1000::196
ns2		IN A	80.88.242.4
ns2		IN AAAA	2a03:1c00:1000::4
ns3		IN A	80.88.242.19
ns3		IN AAAA	2a03:1c00:1000::19
ns		IN A	80.88.242.3
ns		IN A	80.88.242.18
ns		IN A	46.29.56.196
ns		IN AAAA	2a03:1c00:1000::3
ns		IN AAAA	2a03:1c00:1000::18
ns		IN AAAA	2a01:87c0:1000::196
hostmaster	IN A	80.88.242.4
hostmaster	IN AAAA	2a03:1c00:1000::4
; End of Nameservers
;
@       3600    IN TXT  "MS=ms63882057"
; Office 365 Deployment
2connectbahrain.com.                          3600    IN MX           0 2connectbahrain-com.mail.protection.outlook.com.
autodiscover.2connectbahrain.com.             3600    IN CNAME        autodiscover.outlook.com.
2connectbahrain.com.                          3600    IN TXT          "v=spf1 include:spf.protection.outlook.com -all"
_sip._tls.2connectbahrain.com.                3600    IN SRV          100 1 443 sipdir.online.lync.com.
_sipfederationtls._tcp.2connectbahrain.com.   3600    IN SRV          100 1 5061 sipfed.online.lync.com.
sip.2connectbahrain.com.                      3600    IN CNAME        sipdir.online.lync.com.
lyncdiscover.2connectbahrain.com.             3600    IN CNAME        webdir.online.lync.com.
msoid.2connectbahrain.com.                    3600    IN CNAME        clientconfig.microsoftonline-p.net.
enterpriseenrollment.2connectbahrain.com.     3600    IN CNAME        enterpriseenrollment.manage.microsoft.com.
enterpriseregistration.2connectbahrain.com.   3600    IN CNAME	      enterpriseregistration.windows.net.
; End of Office 365 Deployment
;

;
; Email Related
;_domainkey		IN      TXT     "t=y; o=-"
;2connect._domainkey	IN      TXT     "t=y; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnDypcSgbdxqGNiCysXOLId/6FrC0ItLmL1/puT9CiKFPlKshiVcf9chGd6OvdUoQGhE5byu/q/DBv/YY6q1B6aQhqPE8+iNkvoAViTWpCoUSP1sLV1dQzF3A3Xs2QIPw/MVMh9nTPuu34nFLDrkPRCsIJuzkalyN28gGZF9OgnwIDAQAB"

;securemail	IN A	80.88.242.26
;sm		IN A	80.88.242.27
;sm-1		IN A	80.88.242.32
; End of Email Related
;


nat	172800	IN NS	ns1.2connectbahrain.com.
	172800	IN NS	ns2.2connectbahrain.com.

;intranet	IN NS	runner

;vpn		IN A	80.88.240.178
vpn		IN A	80.88.241.126
openvpn		IN A	80.88.242.60

;
; DMZ Servers
cloaker		IN A 80.88.242.18
secure		IN CNAME cloaker
zimbra		IN A	80.88.242.36
mail2		IN A	80.88.242.14
webhost		IN A	80.88.242.44
ftp		IN A	80.88.242.5
www		IN A	80.88.242.5
reports		IN A	80.88.242.6
pay		IN A	80.88.242.12
database	IN A	80.88.242.8
www.db		IN A	80.88.242.6
projects	IN A	80.88.242.5
runner		IN A	80.88.242.4
rabbit		IN A	80.88.242.36
hoster		IN A	80.88.242.44
potter		IN A	80.88.242.40
lcr		IN A	80.88.242.6
cdr		IN A	80.88.242.6
syslog		IN A	80.88.242.40
syslog1		IN A	80.88.242.40
np		IN A	80.88.242.41
np-dev		IN A	80.88.242.28
zcs		IN A	80.88.242.33
strider		IN A	80.88.242.21
webmail		IN A	80.88.242.36
mail		IN A	80.88.242.36
web-devel	IN A	80.88.242.5
pacer		IN A	80.88.242.6
walker		IN A	80.88.242.6
cards		IN A	80.88.242.10
distributor	IN A	80.88.242.10
snmp-trap	IN A	80.88.242.21
tftp		IN A	80.88.242.42
ubuntu-repo	IN A	80.88.242.40
dns-bh-01	IN A	80.88.242.3
dns-bh-01	IN AAAA	2a03:1c00:1000::3
dns-bh-02	IN A	80.88.242.18
dns-bh-02	IN AAAA	2a03:1c00:1000::18
dns-uk-01	IN A	46.29.56.196
dns-uk-01	IN AAAA 2a01:87c0:1000::196
;mail-1		IN A	80.88.242.37
xmpp-01		IN A	80.88.242.37
; End of DMZ Servers
;

ntp		IN A	80.88.242.3

;nms		IN CNAME strider

nms		IN A	80.88.240.30

jogger		IN A	80.88.241.93



; VOIP
;billing		IN A	80.88.247.89
msw1		IN A	80.88.247.102
msw2		IN A	80.88.247.106
genband-rsm	IN A	80.88.247.249


splicecom-cs5100	IN A	80.88.248.251
ipcs		IN A	80.88.244.12
;; ???????????
;gulp		IN A	80.88.242.20


g0-0.3845-bix		IN A 80.88.240.1
f1-0.3845-bix		IN A 80.88.240.22
f1-1.3845-bix		IN A 80.88.240.26
g0-0-v70.3845-bix	IN A 80.88.240.54
g0-1-v536.nbb-7304-2	IN A 80.88.240.2

webcache                IN A 80.88.241.94
webcache-1              IN A 80.88.241.94

git		IN CNAME	bitbucket.org.

;
; Intranet Servers
apps		IN A	192.168.2.92
monitor-2	IN CNAME 	apps
monitor		IN A	192.168.2.33
ipplan		IN A	192.168.2.92
nms-2		IN A	192.168.2.92
dev		IN A	192.168.2.68
stalker		IN A	192.168.3.100
reaper		IN A	192.168.2.50
;jogger		IN A	192.168.2.90
ca		IN A	192.168.3.100
backup		IN A 	192.168.2.29
shaffee		IN A	192.168.2.112
sql2000		IN A	192.168.2.245
opmanager	IN A	192.168.2.100
opmanager-b	IN A	192.168.2.99
logstash-01	IN A	192.168.2.5
logstash-02	IN A	192.168.2.6
logstash	IN CNAME	logstash-01
crm		IN A	192.168.2.18
crm-test	IN A	192.168.2.111
vision		IN A	80.88.248.123
vision		IN A	192.168.16.2
smokeping	IN A	192.168.2.3
dynamips	IN A	192.168.2.21
inventory       IN A    192.168.2.42
netflow		IN A	192.168.2.71
kvm-host-01	IN A	192.168.2.22
kvm-splicecom	IN A	192.168.2.36
pacer		IN A	192.168.2.34
maker		IN A	192.168.50.3
maker14		IN A	192.168.50.6
office14	IN A	192.168.50.6
office		IN A	192.168.50.3
ubuntu-repo	IN A	192.168.2.61
eset		IN A	192.168.50.56
splunk-asa	IN A	192.168.2.12
dns-master	IN A	192.168.2.56
observium	IN A	192.168.2.13
ids		IN A	192.168.2.39
phpipam		IN A	192.168.2.205
phpipam		IN AAAA	2a03:1c00:1:1:214:5eff:fe3e:9194
netdot		IN A	192.168.2.53
netdot		IN AAAA	2a03:1c00:1:1:5054:ff:fee7:6709
solarwinds	IN A	192.168.2.100
zoneminder	IN A	192.168.2.83
nagiosxi	IN A	192.168.2.77
intranet	IN A	192.168.2.28
mq		IN A	192.168.2.31
mail-01		IN A	192.168.2.70
wifi-unifi	IN A	192.168.2.85
wifi-radius-01	IN A	192.168.2.86
wifi-radius-02	IN A	192.168.2.87
cfengine	IN CNAME	cfengine-01
cfengine-01	IN A	192.168.2.4
cfengine-02	IN A	192.168.2.5
;nagiosxi	IN A	192.168.2.76
;nagios		IN A	192.168.2.77
; End of intranet servers

;
; Active Directory Domain
_ldap._tcp.office.2connectbahrain.com. SRV 0 0 389 office.2connectbahrain.com.
_kerberos._tcp.office.2connectbahrain.com. SRV 0 0 88 office.2connectbahrain.com.
_ldap._tcp.dc._msdcs.office.2connectbahrain.com. SRV 0 0 389 office.2connectbahrain.com.
_kerberos._tcp.dc._msdcs.office.2connectbahrain.com. SRV 0 0 88 office.2connectbahrain.com.
; End of Active Directory Domain

;
; Active Directory Domain NEW 2014-04-29
_ldap._tcp.office14.2connectbahrain.com. SRV 0 0 389 office14.2connectbahrain.com.
_kerberos._tcp.office14.2connectbahrain.com. SRV 0 0 88 office14.2connectbahrain.com.
_ldap._tcp.dc._msdcs.office14.2connectbahrain.com. SRV 0 0 389 office14.2connectbahrain.com.
_kerberos._tcp.dc._msdcs.office14.2connectbahrain.com. SRV 0 0 88 office14.2connectbahrain.com.
; End of Active Directory Domain

;
; Virtual Machines
; all VM entries should be listed here
spiceworks		IN A	192.168.50.4
2call			IN A	192.168.2.41	;80.88.242.5
iperf			IN A	192.168.2.44
vxml			IN A	192.168.2.47
splunk			IN A	192.168.2.49
voipswitch-db		IN A	192.168.2.46
development-01		IN A	80.88.242.28
development		IN CNAME	development-01
subversion		IN A	192.168.2.26
gerty			IN A	192.168.2.28
svn			IN A	192.168.2.27
zabbix			IN A	192.168.2.81
itop			IN A	192.168.2.42
;mailscanner		IN A	192.168.7.2
zabbix-server		IN A	192.168.2.13
number-portability-01	IN A	192.168.2.93
number-portability-02	IN A	192.168.2.94
; End of VM entries

;
; HP BladeSystem c7000 Units
; all HP BladeSystem c7000 entries should be listed here
mgmt-a			IN A    192.168.4.50
mgmt-b			IN A    192.168.4.51
blade01                 IN A    192.168.4.101
blade02                 IN A    192.168.4.102
blade03                 IN A    192.168.4.103
blade04                 IN A    192.168.4.104
blade05                 IN A    192.168.4.105
blade06                 IN A    192.168.4.106
blade07                 IN A    192.168.4.107
blade08                 IN A    192.168.4.108
blade09                 IN A    192.168.4.109
blade10                 IN A    192.168.4.110
blade11                 IN A    192.168.4.111
blade12                 IN A    192.168.4.112
blade13                 IN A    192.168.4.113
blade14                 IN A    192.168.4.114
blade15                 IN A    192.168.4.115
blade16                 IN A    192.168.4.116
; End of HP BladeSystem c7000 entries
;

;
; Cisco Service Control Servers
csc10			IN A	80.88.252.10
csc11			IN A	80.88.252.11
csc12			IN A	80.88.252.12
; End of Cisco Service Control Servers
;

;
; Voice Equipment
voip			IN A	80.88.246.25
enum			IN A	80.88.247.23
; End of Voice Equipment
;

cdr-export-switchmgmt   IN A	80.88.242.56
sec			IN A	80.88.242.56


;
; BRAS
bras-radius		IN A	192.168.6.20
;

;
; Mangaement Devices
sw-6513e-02		IN A	192.168.3.154
;

mail.i.2connectbahrain.com.			IN A	80.88.242.37

