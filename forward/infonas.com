$ORIGIN infonas.com.
$TTL 300
@	IN SOA	ns1.infonas.com. hostmaster.infonas.com. (
		2016080401 7200 1200 3600000 7200 )

	; nameserver definitions
	7200	IN NS		ns1.infonas.com.
	7200	IN NS		ns2.infonas.com.
	7200	IN NS		ns3.infonas.com.

;
;  Name Servers 
ns1             IN A    46.29.56.196
ns1             IN AAAA 2a01:87c0:1000::196
ns2             IN A    80.88.242.4
ns2             IN AAAA 2a03:1c00:1000::4
ns3             IN A    80.88.242.19
ns3             IN AAAA 2a03:1c00:1000::19
ns              IN A    80.88.242.3
ns              IN A    80.88.242.18
ns              IN A    46.29.56.196
ns              IN AAAA 2a03:1c00:1000::3
ns              IN AAAA 2a03:1c00:1000::18
ns              IN AAAA 2a01:87c0:1000::196
; End of Name Servers
;

;
; Office 365 Deployment
infonas.com.                            3600    IN MX           0 infonas-com.mail.protection.outlook.com.
autodiscover.infonas.com.               3600    IN CNAME        autodiscover.outlook.com.
infonas.com.                            3600    IN TXT          "v=spf1 include:spf.protection.outlook.com -all"
sip.infonas.com.                        3600    IN CNAME        sipdir.online.lync.com.
lyncdiscover.infonas.com.               3600    IN CNAME        webdir.online.lync.com.
msoid.infonas.com.                      3600    IN CNAME        clientconfig.microsoftonline-p.net.
enterpriseregistration.infonas.com.    	3600    IN CNAME	enterpriseregistration.windows.net.
enterpriseenrollment.infonas.com.      	3600    IN CNAME       	enterpriseenrollment.manage.microsoft.com.
_sip._tls.infonas.com.                  3600    IN SRV          100 1 443 sipdir.online.lync.com.
_sipfederationtls._tcp.infonas.com.     3600    IN SRV          100 1 5061 sipfed.online.lync.com.
; End of Office 365 Deployment
;

; default A record
@	300	IN A	80.88.242.44

; hosts
www			300	IN A	80.88.242.44
blocked 		300	IN A	188.137.140.22
speedtest       	300	IN A    80.88.242.23
